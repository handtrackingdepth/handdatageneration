﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ScreenCapture : MonoBehaviour {

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            print("space key was pressed");
            SaveToTexture();
        }
    }

    void SaveToTexture()
    {
        int w = Screen.width;
        int h = Screen.height;

        Texture2D tex2d = new Texture2D(w, h, TextureFormat.RGB24, false);
        tex2d.ReadPixels(new Rect(0, 0, w, h), 0, 0);
        byte[] imageData = tex2d.EncodeToPNG();

        File.WriteAllBytes(Path.Combine("Assets", "depth_img.png"), imageData);
    }
}
